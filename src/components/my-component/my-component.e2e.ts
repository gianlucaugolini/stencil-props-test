import { newE2EPage } from '@stencil/core/testing';
import { MyComponent } from "./my-component";

describe('my-component', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent('<my-component></my-component>');
    const element = await page.find('my-component');
    expect(element).toHaveClass('hydrated');
  });

  it('should not change props with { mutable: false } option', () => {
    const myComponent = new MyComponent();

    expect(myComponent.mutableFalse).toBe( 'I should not mutate' );

    myComponent.mutableFalse = 'Mutated Text';

    expect(myComponent.mutableFalse).toBe( 'I should not mutate' );

  });

  it('should not change props with no mutable option (default)', () => {
    const myComponent = new MyComponent();

    expect(myComponent.mutableNotSet).toBe( 'I should not mutate' );

    myComponent.mutableNotSet = 'Mutated Text';

    expect(myComponent.mutableNotSet).toBe( 'I should not mutate' );

  });

  it('should change props with { mutable: true } option', () => {
    const myComponent = new MyComponent();

    expect(myComponent.mutableTrue).toBe( 'I should mutate' );

    myComponent.mutableTrue = 'Mutated Text';

    expect(myComponent.mutableTrue).toBe( 'Mutated Text' );

  });

  it('should not change immutable props', () => {
    const myComponent = new MyComponent();

    expect(myComponent.mutableTrue).toBe( 'I should mutate' );
    expect(myComponent.mutableFalse).toBe( 'I should not mutate' );
    expect(myComponent.mutableNotSet).toBe( 'I should not mutate' );

    myComponent.onChange();

    expect(myComponent.mutableTrue).toBe( 'Mutated Text' );
    expect(myComponent.mutableFalse).toBe( 'I should not mutate' );
    expect(myComponent.mutableNotSet).toBe( 'I should not mutate' );

  });

});
