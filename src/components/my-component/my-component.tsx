import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'my-component',
  styleUrl: 'my-component.css',
  shadow: true
})
export class MyComponent {
  @Prop( { mutable: true } ) mutableTrue: string = 'I should mutate';
  @Prop( { mutable: false } ) mutableFalse: string = 'I should not mutate';
  @Prop() mutableNotSet: string = 'I should not mutate';

  onChange() {
    this.mutableTrue = 'Mutated Text';
    this.mutableFalse = 'Mutated Text';
    this.mutableNotSet = 'Mutated Text';
  }

  render() {
    return (
      <div>
        <p>This Prop is declared with "mutable: true": { this.mutableTrue }</p>
        <hr/>
        <p>This Prop is declared with "mutable: false": { this.mutableFalse }</p>
        <hr/>
        <p>This Prop is declared without mutable option set:  { this.mutableNotSet }</p>
        <hr/>
        <button onClick={ this.onChange.bind( this ) }>click here to mutate props</button>
      </div>
    );
  }
}
